/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 */

package com.vmware.connectionapi;

import com.google.gson.Gson;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import com.vmware.connectionapi.utils.BaseApi;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Contains response status code, response, response time and make Request body
 * and get data from pojo class.
 * 
 * @author Mindstix.
 *
 */
public class ResponseHandler {
  private static final Logger LOGGER = LoggerFactory.getLogger(ResponseHandler.class);
  private Response response;
  private String apiName;
  private String endpointUrl;
  private SchemaValidation schemaValidation = new SchemaValidation();

  
  
}