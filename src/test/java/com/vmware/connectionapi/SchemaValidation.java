/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.connectionapi;

import static com.jayway.restassured.RestAssured.given;

import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.jayway.restassured.response.Response;
import com.vmware.connectionapi.test.TestSuiteSetUp;
import com.vmware.connectionapi.utils.BearerToken;
import com.vmware.connectionapi.utils.Constants;
import com.vmware.connectionapi.utils.ExtentMarkup;
import com.vmware.connectionapi.utils.ExtentTestCase;
import com.vmware.connectionapi.utils.HeadersParamsConstants;
import java.io.FileNotFoundException;
import java.io.FileReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

/**
 * JSON Schema validation.
 * 
 * @author Mindstix.
 *
 */

public class SchemaValidation {
  private static final Logger LOGGER = LoggerFactory.getLogger(SchemaValidation.class);
  private Response response;
  private Long responseTime;

  /**
   * JSON Schema validation.
   */
  public void jsonSchemaValidationForRestApi(String apiName, String endpointUrl) {
    LOGGER.info("Schema Validation");
    if (apiName.toUpperCase().contains(Constants.API_METHOD_GET)) {
      LOGGER.info("Executing Schema validation for GET Method");
      jsonSchemaValidationForGetRequestCall(apiName, endpointUrl);
    } else if (apiName.contains(Constants.API_METHOD_POST)) {
      LOGGER.info("Executing Schema validation for POST Method");
      // For now there is not POST request api.
  //    jsonSchemaValidationForPostRequestCall(apiName, endpointUrl);
    }
  }
  
  /**
   * 
   */
  private void getResponeForGetRequestCall(String schemaFilePath, String endpointUrl) {
    response = given().log().all()
        .header(Constants.AUTHORIZATION,
            HeadersParamsConstants.AUTHORIZATION_ALEART + BearerToken.getAccessToken())
        .header(Constants.CONTENT_TYPE, HeadersParamsConstants.CONTENT_TYPE).when()
        .get(endpointUrl).then().statusCode(Constants.STATUSCODE_200).and()
        .body(matchesJsonSchemaInClasspath(schemaFilePath)).extract().response();
    LOGGER.info("Response: {}", response.body().prettyPrint());
    LOGGER.info("Response status code : {} ", response.statusCode());
  }

  /**
   * Schema validation for Get Api call.
   * 
   * @param apiName
   *          : name of the api.
   * @param endpointUrl
   *          : endpoint url of the api.
   * @param schemaFileName
   *          : json scehma file name
   */
  private void jsonSchemaValidationForGetRequestCall(String apiName, String endpointUrl) {
    ExtentTestCase
        .setTest(ExtentTestCase.getExtent().createTest("Schema Validation for :" + apiName));
    LOGGER.info("File Path : {} ", apiName);
    LOGGER.info("File Path: {} ", endpointUrl);
    String requestType = apiName.substring(0, apiName.indexOf("_"));
    String schemaFileName = apiName.substring(apiName.lastIndexOf("_") + 1);
    LOGGER.info("Schema File Name: {} ", schemaFileName);
    String schemaFilePath = "jsonFiles/" + schemaFileName + ".json";
    LOGGER.info("Schema File Path : {}", schemaFilePath);
    LOGGER.info(" In get request call ");
    if (!schemaFileName.isEmpty()) {
      getResponeForGetRequestCall(schemaFilePath,endpointUrl);
      ExtentMarkup.getMarkupBlueLabel("Request Type is : " + requestType + " And Api Name is : " + schemaFileName);
      ExtentMarkup.getMarkupBlueLabel("Service Endpoint Url : " +endpointUrl);
      ExtentTestCase.getTest().info("Actual Response Status : " + response.statusCode()
          + " Expected Response Status Code : " + Constants.STATUSCODE_200);
      ExtentTestCase.getTest().pass("JSON Schema matched");
      ExtentMarkup.getMarkupCodeBlock("[Passed] API Response: " + response.body().prettyPrint());
      LOGGER.info("JSON Schema matched ");    
    } else {
      ExtentTestCase.getTest().info("Schema File Not Present");
      LOGGER.info("Schema File Not Present");
    }
  }
}
