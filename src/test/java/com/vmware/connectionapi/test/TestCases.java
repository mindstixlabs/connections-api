/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 */

package com.vmware.connectionapi.test;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.jayway.restassured.response.Response;
import com.vmware.connectionapi.SchemaValidation;
import com.vmware.connectionapi.utils.BaseApi;
import com.vmware.connectionapi.utils.Constants;
import com.vmware.connectionapi.utils.ExtentManager;
import com.vmware.connectionapi.utils.ExtentMarkup;
import com.vmware.connectionapi.utils.ExtentTestCase;
import com.vmware.connectionapi.utils.RestAssuredUtility;

/**
 * Execute all test cases.
 * 
 * @author Mindstix
 */
public class TestCases {

  private static final Logger LOGGER = LoggerFactory.getLogger(TestCases.class);
  private Response response;
  private Long responseTime;
  private int responseStatusCode;
  private BaseApi baseapi = new BaseApi();
  private SchemaValidation schemaValidation = new SchemaValidation();

  /**
   * Set the Base URI and Path.
   * 
   */
  @BeforeClass(groups = { "regression", "smoke" })
  public void setup() {
    LOGGER.info("Setup baseUrl and basePath for connection app api");
    RestAssuredUtility.setBaseUri(TestSuiteSetUp.vmwareEnvProps.getProperty("baseURL"));
    RestAssuredUtility.setBasePath(TestSuiteSetUp.vmwareEnvProps.getProperty("basePath"));
    ExtentTestCase.setExtent(ExtentManager.getExtent());
  }

  /**
   * Contains a endpoint url : list of connection api.
   * 
   */
  @DataProvider(name = "connectionapilist")
  public Object[][] feedbackApiList() throws InterruptedException {
    LOGGER.info("In Connection Api DataProvider ");
    return Constants.apiName;
  }

  /**
   * Contains a false service endpoint url list of connection api.
   * 
   */
  @DataProvider(name = "falseUrlapilist")
  public Object[][] feedbackApiListForFalseUrl() throws InterruptedException {
    LOGGER.info("In Connection Api DataProvider ");
    return Constants.falseApiName;
  }

  /**
   * To verify 200 status code and response time.
   */
  @Test(groups = { "regression",
      "smoke" }, dataProvider = "connectionapilist", priority = 1, enabled = true)
  public void verifyStatusCodesAndResTime(String apiName, String extendedUrl) {
    LOGGER.info("<----------- Executing first test case ------------>");
    ExtentTestCase.setTest(ExtentTestCase.getExtent().createTest(
        "Verify Response Status Code and Response Time for Api " + apiName,
        "Enpoint url is : " + extendedUrl));
    response = baseapi.getResponseForAllTheApi(Constants.API_METHOD_GET, apiName, extendedUrl);
    responseStatusCode = baseapi.getStatusCode(response, apiName, extendedUrl);
    LOGGER.info("Response Time of : {} : {} ms ", apiName, responseTime);
    ExtentTestCase.getTest().info("Actual Response Status : " + responseStatusCode
        + "  Expected Response Status Code : " + Constants.STATUSCODE_200);
    Assert.assertEquals(responseStatusCode, Constants.STATUSCODE_200, "Status code should be 200");
    LOGGER.info("<----------- Executed first test case ------------>");
  }

  /**
   * To verify schema validation for all APIs.
   */
  @Test(groups = { "regression" }, dataProvider = "connectionapilist", priority = 2, enabled = true)
  public void verifySchemaValidation(String apiName, String extendedUrl) {
    LOGGER.info("<----------- Executing 2nd test case -----------> ");
    schemaValidation.jsonSchemaValidationForRestApi(apiName, extendedUrl);
    LOGGER.info("<----------- Executed 2nd test case -----------> ");
  }

  /**
   * To check 401 (Unauthorized) status code.
   */
  @Test(groups = {
      "regression" }, dataProvider = "connectionapilist", priority = 4, enabled = true)
  public void verifyUnauthorizedStatusCode(String apiName, String extendedUrl) {
    LOGGER.info("<----------- Executing 4th test case ------------>");
    ExtentTestCase.setTest(ExtentTestCase.getExtent().createTest(
        "Verify 401 Unauthorized status code for : " + apiName, "Enpoint url is : " + extendedUrl));
    response = baseapi.getResponseForAllTheApi(Constants.UNAUTH_GET, apiName, extendedUrl);
    responseStatusCode = baseapi.getStatusCode(response, apiName, extendedUrl);
    ExtentTestCase.getTest().info("Actual Response Status : " + responseStatusCode
        + "  Expected Response Status Code : " + Constants.STATUSCODE_401);
    Assert.assertEquals(responseStatusCode, Constants.STATUSCODE_401, "Status code should be 401");
    LOGGER.info("<----------- Executing 4th test case ------------>");
  }

  /**
   * To verify 404 status code.
   */
  @Test(groups = { "regression" }, dataProvider = "falseUrlapilist", priority = 5, enabled = true)
  public void verifyPageNotFoundStatusCodes(String apiName, String extendedUrl)
      throws JsonParseException, JsonMappingException, IOException {
    LOGGER.info("<----------- Executing 5th test case ------------>");
    ExtentTestCase.setTest(ExtentTestCase.getExtent().createTest(
        " Verify 404 status code for  : " + apiName, "Enpoint url is : " + extendedUrl));
    response = baseapi.getResponseForAllTheApi(Constants.API_METHOD_GET, apiName, extendedUrl);
    responseStatusCode = baseapi.getStatusCode(response, apiName, extendedUrl);
    ExtentTestCase.getTest().info("Actual Response Status : " + responseStatusCode
        + "  Expected Response Status Code : " + Constants.STATUSCODE_404);
    Assert.assertEquals(responseStatusCode, Constants.STATUSCODE_404, "Status code should be 200");
    LOGGER.info("<----------- Executed 5th test case ------------>");
  }

  /**
   * AfterTest Method to get the Test Result.
   */
  @AfterMethod(groups = { "regression", "smoke"})
  public void getResult(ITestResult result) {
    LOGGER.info("Recording Results");
    if (result.getStatus() == ITestResult.FAILURE) {
      ExtentTestCase.getTest().fail(Status.FAIL + ", Failed TestCase is " + result.getName());
      ExtentTestCase.getTest().fail(Status.FAIL + ", Failed TestCase is " + result.getThrowable());
    } else if (result.getStatus() == ITestResult.SKIP) {
      ExtentTestCase.getTest().skip(Status.SKIP + ", Skipped TestCase is " + result.getName());
    } else {
      ExtentTestCase.getTest().pass(Status.PASS + ",  Test Case Method : " + result.getName());
      ExtentMarkup.getMarkupBlueLabel("Test Case Passed");
    }
    ExtentTestCase.getExtent().flush();
  }

  /**
   * Reset base URI and Base path.
   */
  @AfterClass
  public void setDown() {
    RestAssuredUtility.resetBaseUri();
    RestAssuredUtility.resetBasePath();
  }

}
