/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.connectionapi.utils;

import static com.jayway.restassured.RestAssured.given;
import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.jayway.restassured.response.Response;
import com.vmware.connectionapi.test.TestSuiteSetUp;
import com.vmware.connectionapi.utils.Constants;
import com.vmware.connectionapi.utils.ExtendedUri;
import com.vmware.connectionapi.utils.HeadersParamsConstants;
import io.github.bonigarcia.wdm.ChromeDriverManager;


/**
 * Login and get the Authentication code.
 * 
 * @author Mindstix
 * 
 */

public class BearerToken {
  // Declaring all the private and public members
  private static final Logger LOGGER = LoggerFactory.getLogger(BearerToken.class);
  private WebDriver driver;
  private static String refreshToken;
  private static String accessToken;
  private String authCode;

  public static String getRefreshToken() {
    return refreshToken;
  }

  public static void setRefreshToken(String refreshToken) {
    BearerToken.refreshToken = refreshToken;
  }

  public static void setAccessToken(String accessToken) {
    BearerToken.accessToken = accessToken;
  }

  public static String getAccessToken() {
    return accessToken;
  }

  private String loginUrl = TestSuiteSetUp.vmwareEnvProps.getProperty("baseURI_Auth")
      + TestSuiteSetUp.vmwareEnvProps.getProperty("basePath_Auth") + ExtendedUri.EXTENDED_LOGIN_URI;
 
  
  /**
   * Set up for to launch Chrome browser through dockerIP and set the Base URI and Path.
   * 
   */
  public void setup(String dockerIp, String portNum) throws MalformedURLException {
    int port = Integer.parseInt((String) portNum);
    ChromeDriverManager.getInstance().setup();
    ChromeOptions co = new ChromeOptions();
    co.addArguments("--headless");
    co.addArguments("test-type");
    co.addArguments("disable-infobars");
    DesiredCapabilities cap = DesiredCapabilities.chrome();
    cap.setCapability(ChromeOptions.CAPABILITY, co);
    if (port == 0) {
      driver = new ChromeDriver(co);
    } else {
      URL url = new URL("http://" + dockerIp + ":" + port + "/wd/hub");
      LOGGER.info("Setting remote driver url as : {}", url);
      driver = new RemoteWebDriver(url, cap);
    }
    LOGGER.info("loginUrl : {}", loginUrl);
    driver.get(loginUrl);
  }

  /**
   * Set up for to launch Chrome browser and set the Base URI and Path.
   * 
   */
  public void setup() {
    ChromeDriverManager.getInstance().setup();
    driver = new ChromeDriver();
    driver.get(loginUrl);
    LOGGER.info("Login Url : {} ", loginUrl);
  }

  /**
   * Selection of domain from the dropdown list : vmware.
   *
   */
  public void selectionOfDomain() {
    WebDriverWait wait = new WebDriverWait(driver, 90);
    LOGGER.info("Select the domain");
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(Constants.DROPDOWN_ID)));
    driver.findElement(By.id(Constants.DROPDOWN_ID)).sendKeys(Constants.DROPDOWN_VALUE);
    driver.findElement(By.id(Constants.NEXT_ID)).click();
  }

  /**
   * Login to the application.
   * 
   */
  public void loginValidCred() {
    // Enter user name
    WebDriverWait wait = new WebDriverWait(driver, 90);
    LOGGER.info("Entering name");
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
    driver.findElement(By.id("username")).clear();
    driver.findElement(By.id("username")).sendKeys(RestAssuredUtility.decode(Constants.USER_NAME));
    LOGGER.info("Entered username ");
    // Enter Password
    driver.findElement(By.id("password")).clear();
    driver.findElement(By.id("password"))
        .sendKeys(RestAssuredUtility.decode(Constants.USER_PASSWORD));
    LOGGER.info("Entered password");
    // Click on sign in button
    driver.findElement(By.id("signIn")).click();
    LOGGER.info("Clicked on sign in button");
    // Wait till page load and click on grant button
    String currentUrl = driver.getCurrentUrl();
    LOGGER.info("Current URL: {} ", currentUrl);
    if ((currentUrl.contains(Constants.REDIRECT_URI))) {
      WebElement grantButton = driver.findElement(By.xpath("//*[@id='authorize']"));
      wait.until(ExpectedConditions.elementToBeClickable(grantButton));
      grantButton.click();
    }
    authCode = getAuthCode();
  }

  /**
   * Get authentication code.
   */
  private String getAuthCode() {
    // Return Authentication code from the URL
    String url = driver.getCurrentUrl();
    LOGGER.info("URL: {}", url);
    String authCode = null;
    if (url.contains("code")) {
      authCode = url.substring(url.lastIndexOf("=") + 1);
      LOGGER.info("Authetication Code: {}", authCode);
    }
    return authCode;
  }

  /**
   * Generate refresh token and access token.
   */
  public void generateToken() {
   
    // extract request access token from the code.
    String accessTokenUrl = TestSuiteSetUp.vmwareEnvProps.getProperty("baseURI_Token")
        + TestSuiteSetUp.vmwareEnvProps.getProperty("basePath_Token")+ExtendedUri.EXTENDED_ACCESS_TOKEN_URI_1 + authCode
        + ExtendedUri.EXTENDED_ACCESS_TOKEN_URI_2;
    LOGGER.info("acessTokenURL: {}", accessTokenUrl);
    Response res;
    // @formatter:off
    res = given()
        .log()
        .all()
        .header(Constants.AUTHORIZATION, HeadersParamsConstants.AUTHORIZATION).when()
        .post(accessTokenUrl)
        .then()
        .statusCode(Constants.STATUSCODE_200)
        .extract()
        .response();
    LOGGER.info("Response is : {} ", res.toString());
    // @formatter:on
    // get refresh token
    setRefreshToken(res.getBody().jsonPath().getString("refresh_token"));
    LOGGER.info("Refresh Token : {} ", getRefreshToken());
    setAccessToken(res.getBody().jsonPath().getString("access_token"));
    LOGGER.info("Access Token: {} ", getAccessToken());
  }

  /**
   * Quit driver, Reset base URI and Base path.
   */
  public void tearDown() {
    LOGGER.info("tearDown :");
    driver.close();
  }
}
