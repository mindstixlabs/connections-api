/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.connectionapi.utils;

/**
 * Constants.
 * 
 * @author Mindstix.
 *
 */
public final class Constants {
  public static final String USER_NAME="c2hhcm1hYW5raXRh";
  public static final String USER_PASSWORD ="TWluZHN0aXhAMTIzNDU="; 
  public static final String AUTHORIZATION = "Authorization";
  public static final String CONTENT_TYPE = "Content-Type";
  public static final String WRONG_CONTENT_TYPE = "aaaa";
  public static final String REDIRECT_URI = "redirect_uri";
  public static final String ACCEPT = "Accept";
  public static final String USER = "user";
  public static final String REFRESHTOKENNAME = "BearerToken";
  public static final String DROPDOWN_ID = "userStoreDomain";
  public static final String DROPDOWN_VALUE = "vmware";
  public static final String NEXT_ID = "userStoreFormSubmit";
  public static final String APPNAME = "appName";
  public static final String HZN_COOKIE = "HZN";
  public static final String API_METHOD_GET = "GET";
  public static final String API_METHOD_POST = "POST";
  public static final String UNAUTH_GET = "unauthorizedGET";
  public static final String NOT_ACCEPT_GET = "notAcceptableGET";
  public static final String NEW_ACCESS_TOKEN = "newAccessToken";
  public static final String METHOD_NOT_ALLOWED = "methodNotAllowed";
  public static final String FILTER= "filter";
  public static final int EXP_RESP_TIME3 = 300;
  public static final int EXP_RESP_TIME5 = 5000;
  public static final int EXP_RESP_TIME10 = 10000;
  public static final int STATUSCODE_400 = 400;
  public static final int STATUSCODE_401 = 401;
  public static final int STATUSCODE_404 = 404;
  public static final int STATUSCODE_405 = 405;
  public static final int STATUSCODE_406 = 406;
  public static final int STATUSCODE_200 = 200;


  public static final String[][] apiName = {
      {"GET_Announcements","/getAnnouncements?username=sharmaankita@vmware.com"},
      {"GET_GlobalCommunication","/getGlobalCommunication?username=sharmaankita@vmware.com"},
      {"GET_Hive","/empEnrollmentDetails?username=sharmaankita@vmware.com"},
     {"GET_UserDetails","/getUserDetails?username=sharmaankita@vmware.com"},
      {"GET_KeywordSearch","/gsaKeySearchDetails?searchKey=take"},     
  };
    
  public static final String[][] falseApiName = {
      {"GET_Announcements","/getAnnoncements?username=sharmaankita@vmware.com"},
      {"GET_GlobalCommunication","/getGlobalCommuicaton?username=sharmaankita@vmware.com"},
      {"GET_UserDetails","/getUerDetails?username=sharmaankita@vmware.com"},
      {"GET_KeywordSearch","/gsaKeySearchDetils?searchKey=take"},
      {"GET_Hive","/empEnrollentDetails?username=sharmaankita@vmware.com"}    
  };
  
}
