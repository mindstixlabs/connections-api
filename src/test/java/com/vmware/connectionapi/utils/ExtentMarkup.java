/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.connectionapi.utils;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;

/**
 * This class contains methods to add Markup to extent report.
 * 
 * @author Mindstix
 */
public class ExtentMarkup {
  static Markup label;

  /**
   * Method to add a blue label to Extent Report.
   * 
   * @param labelText
   */
  public static void getMarkupBlueLabel(String labelText) {
    ExtentTestCase.getTest().log(Status.INFO, MarkupHelper.createLabel(labelText, ExtentColor.BLUE).getMarkup());
  }
  
  /**
   * Method to add a cyan color label to Extent Report.
   * 
   * @param labelText
   */
  public static void getMarkupCyanLabel(String labelText) {
    ExtentTestCase.getTest().log(Status.INFO, MarkupHelper.createLabel(labelText, ExtentColor.RED).getMarkup());
  }

  /**
   * Method to add a data table to Extent Report.
   * 
   * @param tableData
   */
  public static void getMarkupTable(String[][] tableData) {
    ExtentTestCase.getTest().log(Status.INFO, MarkupHelper.createTable(tableData).getMarkup());
  }

  /**
   * Method to add code to Extent Report.
   * 
   * @param code
   */
  public static void getMarkupCodeBlock(String code) {
    ExtentTestCase.getTest().log(Status.INFO, MarkupHelper.createCodeBlock(code).getMarkup());
  }

}
