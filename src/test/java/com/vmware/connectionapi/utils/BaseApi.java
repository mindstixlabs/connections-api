/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.connectionapi.utils;

import static com.jayway.restassured.RestAssured.given;
import com.jayway.restassured.response.Response;
import com.vmware.connectionapi.utils.BearerToken;
import com.vmware.connectionapi.utils.Constants;
import com.vmware.connectionapi.utils.HeadersParamsConstants;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Contains response, response status code , response time.
 * 
 * @author Mindstix
 *
 */
public class BaseApi {
  private static final Logger LOGGER = LoggerFactory.getLogger(BaseApi.class);
  private Response response;
  protected Long responseTime;
  private String newAccessToken;
  private String newRefreshToken;
  public int responseStatusCode;
  public Boolean isResponseTimeValid;
  public boolean isNewAccessToken = false;
  private Boolean isRefreshTokenApiExecuteSingleTime = true;

  public enum WebApiMethodCall {
    GET, unauthorizedGET, methodNotAllowed, badRequest, newAccessToken;
  }

  /**
   * This method is used to get response for all the APIs.
   */
  public Response getResponseForAllTheApi(String webApiMethodName, String apiName,
      String extendedUrl) {
    LOGGER.info("File Path: {}", apiName);
    LOGGER.info("File Path: {}", extendedUrl);
    LOGGER.info("Method name: {}", webApiMethodName);
    WebApiMethodCall apiMethodName = WebApiMethodCall.valueOf(webApiMethodName);

    switch (apiMethodName)
    {
      case GET:
        LOGGER.info("In Get call");
        // Api for 200 , 404, 405 status code.
        response = getResponseOfGetRequestCall(apiName, extendedUrl);
        break;
      case unauthorizedGET:
        LOGGER.info("Get call for unauthorized Api");
        response = getUnauthorizedResponseOfGetRequestCall(apiName, extendedUrl);
        break;
      default:
        LOGGER.error("Invalid Request call");
    }
    return response;
  }

  /**
   * This method is used to get response for get call. For new access token and
   * old access token.
   * 
   * @return : response
   */
  private Response getResponseOfGetRequestCall(String getCallApiName, String getCallEndPointUrl) {
    if (isNewAccessToken) {
      LOGGER.info(" Executing for new Access token ");
      response = given().log().all()
          .header(Constants.AUTHORIZATION,
              HeadersParamsConstants.AUTHORIZATION_ALEART + newAccessToken)
          .header(Constants.CONTENT_TYPE, HeadersParamsConstants.CONTENT_TYPE).when()
          .get(getCallEndPointUrl).then().extract().response();
    } else {
      LOGGER.info(" Executing for older Access token ");
      response = given().log().all()
          .header(Constants.AUTHORIZATION,
              HeadersParamsConstants.AUTHORIZATION_ALEART + BearerToken.getAccessToken())
          .header(Constants.CONTENT_TYPE, HeadersParamsConstants.CONTENT_TYPE).when()
          .get(getCallEndPointUrl).then().extract().response();
    }
    responseTime = response.time();
    LOGGER.info("Response time for Get api {} : {} ms ", getCallApiName, responseTime);
    return response;
  }

  /**
   * This method is used to get response for post call of refresh token api and
   * generate new Access Token Value.
   * 
   * @refreshTokenApiName : name of refresh token api.
   * @refreshTokenEndPointUrl : endpoint url of refresh token api.
   */
  public void generateNewAccessToken(String refreshTokenApiName, String refreshTokenUrl) {
    LOGGER.info("Refresh Token : {}", (BearerToken.getRefreshToken()));
    if (isRefreshTokenApiExecuteSingleTime) {
      response = given().log().all()
          .header(Constants.AUTHORIZATION, HeadersParamsConstants.AUTHORIZATION)
          .header(Constants.CONTENT_TYPE, HeadersParamsConstants.CONTENT_TYPE).when()
          .post(refreshTokenUrl).then().statusCode(Constants.STATUSCODE_200).extract().response();
      LOGGER.info("Response after refresh token : {} ", response.toString());
      newAccessToken = response.getBody().jsonPath().getString("access_token");
      LOGGER.info("New Access Token : {} ", newAccessToken);
      newRefreshToken = response.getBody().jsonPath().getString("refresh_token");
      LOGGER.info("New Refresh Token : {} ", newRefreshToken);
      LOGGER.info("Response of {} : {} ", refreshTokenApiName, response.body().prettyPrint());
      isRefreshTokenApiExecuteSingleTime = false;
    }
  }

  /**
   * This method is used to get response for get call without passing bearer
   * token.
   * 
   * @param unauthorizedApiName
   *          : Name of the Api.
   * @param unauthorizedEndpointUrl
   *          :Endpoint Url of the Api.
   * @return : response
   */
  private Response getUnauthorizedResponseOfGetRequestCall(String unauthorizedApiName,
      String unauthorizedEndpointUrl) {
    LOGGER.info("unauthorizedEndpointUrl : {} ", unauthorizedEndpointUrl);
    response = given().log().all().header(Constants.AUTHORIZATION, "")
        .header(Constants.CONTENT_TYPE, HeadersParamsConstants.CONTENT_TYPE)
        .header(Constants.ACCEPT, HeadersParamsConstants.ACCEPT).when().get(unauthorizedEndpointUrl)
        .then().extract().response();
    LOGGER.info("Response of  : {} ", response.body().prettyPrint());
    LOGGER.info("Status Code: {}", response.statusCode());
    return response;
  }

  /**
   * This method is used to get status code for all the APIs.
   */
  public int getStatusCode(Response response, String apiName, String endpointUrl) {
    String responseStatusCode = Integer.toString(response.statusCode());
    LOGGER.info("Calling : {} Api  : Response status code : {} ", apiName, responseStatusCode);
    String requestType = apiName.substring(0, apiName.indexOf("_"));
    apiName = apiName.substring(apiName.lastIndexOf("_") + 1);
    ExtentTestCase.getTest()
        .info("Request Type is : " + requestType + " And Api Name is : " + apiName);
    ExtentTestCase.getTest().info("Service Endpoint Url : " + endpointUrl);
    ExtentTestCase.getTest().info("Response Status Code : " + responseStatusCode);
    ExtentTestCase.getTest().info("Response Time : " + response.time() + " ms");
    ExtentMarkup.getMarkupCodeBlock("Api Response : " + response.body().prettyPrint());     
    return response.statusCode();
  }

}