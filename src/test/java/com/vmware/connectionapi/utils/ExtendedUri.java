/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.connectionapi.utils;


/**
 * Setting extended URIs.
 * 
 * @author Mindstix
 *
 */

public final class ExtendedUri {
  public static final String EXTENDED_LOGIN_URI = "/authorize?response_type=code&redirect_uri=https://mbei.vmware.com:9090/auth-server/access_response&client_id=direct_access_app";
  public static final String EXTENDED_ACCESS_TOKEN_URI_1 ="/token?grant_type=authorization_code&code=";
  public static final String EXTENDED_ACCESS_TOKEN_URI_2 ="&redirect_uri=https://mbei.vmware.com:9090/auth-server/access_response&scope=read"; 
  public static final String REFRESH_TOKEN_URI = "/token?grant_type=refresh_token&refresh_token="
      + BearerToken.getRefreshToken() + ExtendedUri.EXTENDED_ACCESS_TOKEN_URI_2;
}
